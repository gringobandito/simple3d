//
// Created by nason on 30.09.2017.
//

#ifndef GLEWIMPORTER_H
    #define GLEWIMPORTER_H

    #ifndef GLEW_STATIC
        #define GLEW_STATIC
    #endif

    #include "GL/glew.h" // Before any gl headers

#endif //GLEWIMPORTER_H
