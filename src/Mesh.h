#ifndef MESH_H
#define MESH_H
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
using namespace std;
#include "GLEWImporter.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


//Assimp
#include <assimp/types.h>

#include "Shader.h"
#include "resources/Texture2D.h"
#include "Material3D.h"
#include "materials/MaterialBase.h"
#include <memory>
#include "Pivot3D.h"



enum AttribPointer{
    VERTEX_ID_LOCATION = 0,
    NORMAL_ID_LOCATION = 1,
    UV_ID_LOCATION = 2,
    BONE_ID_LOCATION = 3,
    BONE_WEIGHT_LOCATION = 4
};

struct Vertex {
	// Position
	glm::vec3 Position;
	// Normal
	glm::vec3 Normal;
	// TexCoords
	glm::vec2 TexCoords;
};

#define NUM_BONES_PER_VEREX 4
struct VertexBoneData
{
    unsigned int IDs[NUM_BONES_PER_VEREX];
    float Weights[NUM_BONES_PER_VEREX];
};

#include "ExternalModel.h"

class Mesh: public Pivot3D {
private:
    bool _hasBones = false;

    /*  Render data  */
    GLuint vertexAttributesArray;
    GLuint vertexArrayBuffer;
    GLuint elementArrayBuffer;
    GLuint boneArrayBuffer;

    /*  Functions    */
    // Initializes all the buffer objects/arrays
    void setupMesh();
protected:
    void applyTransformRotation() override;
public:
	/*  Mesh Data  */
    vector<Vertex> _vertices;
    vector<GLuint> _indices;
    vector<VertexBoneData> _bones;
    std::shared_ptr<MaterialBase> _material;

	/*  Functions  */
	// Constructor
    Mesh(vector<Vertex> vertices, vector<GLuint> indices, std::shared_ptr<MaterialBase> mat, std::vector<VertexBoneData> bones);
	~Mesh();
	// Render the mesh
    void render(std::shared_ptr<MaterialBase> material) override;
    std::shared_ptr<MaterialBase> material() const;
};

#endif//MESH_H
