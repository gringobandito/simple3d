#pragma once

#ifndef TextureManager_h__
#define TextureManager_h__
#include <map>
#include <string>
#include "GLEWImporter.h"
#include <SOIL/SOIL.h>
#include <assimp/types.h>
#include "resources/Texture2D.h"
#include <memory>

class TextureManager
{
private:
    std::map<std::string, std::shared_ptr<Texture2D>> _textures;

public:
	TextureManager();
	~TextureManager();
    std::shared_ptr<Texture2D> loadTexture(std::string name);
    std::shared_ptr<Texture2D> getTexture(std::string texturePath, std::string typeName, std::string directory);
    static GLint TextureFromFile(std::string filename, std::string directory)
	{
		//Generate texture ID and load texture data 
        std::string fullname = directory + '/' + filename;
		GLuint textureID;
		glGenTextures(1, &textureID);
		int width, height;
        unsigned char* image = SOIL_load_image(fullname.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
		// Assign texture to ID
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);

		// Parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
		SOIL_free_image_data(image);
		return textureID;
	}
protected:
	
private:
};

#endif // TextureManager_h__
