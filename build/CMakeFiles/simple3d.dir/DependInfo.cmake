# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "D:/_Projects/simple3d/include/GL/glew.c" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/include/GL/glew.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "../src"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/_Projects/simple3d/GLFWKeyboardInput.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/GLFWKeyboardInput.cpp.obj"
  "D:/_Projects/simple3d/GLFWMouseInput.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/GLFWMouseInput.cpp.obj"
  "D:/_Projects/simple3d/main.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/main.cpp.obj"
  "D:/_Projects/simple3d/src/BoxModel.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/BoxModel.cpp.obj"
  "D:/_Projects/simple3d/src/Device3D.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/Device3D.cpp.obj"
  "D:/_Projects/simple3d/src/Engine.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/Engine.cpp.obj"
  "D:/_Projects/simple3d/src/ExternalModel.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/ExternalModel.cpp.obj"
  "D:/_Projects/simple3d/src/GLUtils.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/GLUtils.cpp.obj"
  "D:/_Projects/simple3d/src/Material3D.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/Material3D.cpp.obj"
  "D:/_Projects/simple3d/src/Mesh.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/Mesh.cpp.obj"
  "D:/_Projects/simple3d/src/Model.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/Model.cpp.obj"
  "D:/_Projects/simple3d/src/Pivot3D.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/Pivot3D.cpp.obj"
  "D:/_Projects/simple3d/src/Scene3D.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/Scene3D.cpp.obj"
  "D:/_Projects/simple3d/src/Shader.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/Shader.cpp.obj"
  "D:/_Projects/simple3d/src/UpdateBroadcaster.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/UpdateBroadcaster.cpp.obj"
  "D:/_Projects/simple3d/src/camera/Camera.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/camera/Camera.cpp.obj"
  "D:/_Projects/simple3d/src/camera/FreeLookCamera.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/camera/FreeLookCamera.cpp.obj"
  "D:/_Projects/simple3d/src/input/KeyboardInput.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/input/KeyboardInput.cpp.obj"
  "D:/_Projects/simple3d/src/input/MouseInput.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/input/MouseInput.cpp.obj"
  "D:/_Projects/simple3d/src/materials/ColorMaterial.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/materials/ColorMaterial.cpp.obj"
  "D:/_Projects/simple3d/src/materials/MaterialBase.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/materials/MaterialBase.cpp.obj"
  "D:/_Projects/simple3d/src/materials/ObjectIdMaterial.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/materials/ObjectIdMaterial.cpp.obj"
  "D:/_Projects/simple3d/src/materials/ShaderFactory.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/materials/ShaderFactory.cpp.obj"
  "D:/_Projects/simple3d/src/object_selector/ObjectSelector.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/object_selector/ObjectSelector.cpp.obj"
  "D:/_Projects/simple3d/src/render/RenderModeHelper.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/render/RenderModeHelper.cpp.obj"
  "D:/_Projects/simple3d/src/resources/Texture2D.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/resources/Texture2D.cpp.obj"
  "D:/_Projects/simple3d/src/resources/TextureManager.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/resources/TextureManager.cpp.obj"
  "D:/_Projects/simple3d/src/utils/FileUtils.cpp" "D:/_Projects/simple3d/cmake-build-debug/CMakeFiles/simple3d.dir/src/utils/FileUtils.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
