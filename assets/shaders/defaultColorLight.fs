#version 330 core

struct Material {
	//sampler2D diffuse;
    //vec3 specular;    
    float shininess;
}; 

struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};



out vec4 color;

in vec3 FragPos;  
in vec3 Normal;  
in vec2 TexCoords;
  
uniform vec3 viewPos;

uniform Material material;
uniform Light light;
uniform vec4 uColor;

void main()
{
	// Ambient
    vec3 ambient = light.ambient * vec3(uColor.rgb);
  	
    // Diffuse 
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * vec3(uColor.rgb);
    
    // Specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * spec;
        
    vec3 result = ambient + diffuse + specular;
    color = vec4(result, 1.0f);
	
}