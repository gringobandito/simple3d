#The places to look for the tinyxml2 folders
#set(FIND_GLWF3_PATHS
#        D:/_Projects/OpenglCLionSetup                         #On Windows, this is where my tinyxml2 folder is
#        )

#The location of the include folder (and thus the header files)
#find_path uses the paths we defined above as places to look
#Saves the location of the header files in a variable called TINYXML2_INCLUDE_DIR
find_path(SOIL_INCLUDE_DIR SOIL/SOIL.h   #The variable to store the path in and the name of the header files
        PATH_SUFFIXES include              #The folder name containing the header files
        PATHS ${PROJECT_SOURCE_DIR}/include/)       #Where to look (defined above)
message("Running FindSOIL")

IF(APPLE)
    message("Finding SOIL for APPLE")
    find_library(SOIL_LIBRARY
            NAMES SOIL
            PATHS ${PROJECT_SOURCE_DIR}/libs/SOIL/osx)
ELSE(APPLE) #Windows PS
    find_library(SOIL_LIBRARY
            NAMES SOIL
            PATHS ${PROJECT_SOURCE_DIR}/libs/SOIL)
ENDIF(APPLE)


SET(SOIL_FOUND "NO")
IF(SOIL_LIBRARIES AND SOIL_INCLUDE_DIRS)
    SET(SOIL_FOUND "YES")
ENDIF(SOIL_LIBRARIES AND SOIL_INCLUDE_DIRS)